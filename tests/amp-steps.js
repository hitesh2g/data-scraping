require("dotenv").config({ path: ".env" });
var amphtmlValidator = require('amphtml-validator');
const request = require('request');
const fs = require("fs");
const { BASE_URL } = process.env || {};
process.env.NODE_TLS_REJECT_UNAUTHORIZED = false

step("Validate amp page for this <relativeUrl> url", async function (relativeUrl) {
    async function validateURL(url) {
      return new Promise((resolve, reject) => {
        request({url, rejectUnauthorized: false}, function (error, response, body) {
          if(response.statusCode != 404){
            if (body) {
              amphtmlValidator.getInstance().then(function (validator) {
                var result = validator.validateString(body);
                if(result.status == "PASS"){
                  fs.appendFileSync("amp-validation-status.txt", `\nIsAMP: ${result.status}  URL: ${url}`);
                  console.log(`IsAMP: ${result.status}  URL: ${url}`)
                }else if(result.status == "FAIL"){ 
                  fs.appendFileSync("amp-validation-status.txt", `\nIsAMP: ${result.status}  URL: ${url}`);
                  for(let i = 0; i < result.errors.length; i++){
                    fs.appendFileSync("amp-validation-status.txt", `  Error-at-line: ${result.errors[i].line} : ${result.errors[i].message}`);                    
                    console.log(`IsAMP: ${result.status}  URL: ${url}  Error-line: ${result.errors[i].line} Error-message: ${result.errors[i].message}`)
                  }
                }
                resolve();
              });
            } else if (error) {
              console.log(error)
              fs.appendFileSync("amp-validation-status.txt", `\nERROR: ${error} URL: ${url} `);
              reject(error)
            }
          }else {
            fs.appendFileSync("amp-validation-status.txt", `\nIsAMP: ${response.statusCode}  URL: ${url}`);
            console.log(`IsAMP: ${response.statusCode}  URL: ${url}`)
            resolve();
          }
        });
      })
  
    }
  
    if (BASE_URL.includes("master.dev")) {
      if (relativeUrl.charAt(relativeUrl.length - 1) === "/")
        relativeUrl = relativeUrl.substring(0, relativeUrl.length - 1);
      relativeUrl = `${relativeUrl}?fcsis=stage`;
    }
    const url = `${BASE_URL}${relativeUrl}`;
    if(url.includes(".html")){
      await validateURL(url)
    }
  });