require("dotenv").config({ path: ".env" });

const puppeteer = require("puppeteer");
const assert = require("assert");
const path = require("path");
const puppeteerUtil = require("./../utils/puppeeterUtil");
const fileSystemUtil = require("./../utils/fileSystemUtil");
const { BASE_URL } = process.env || {};
const siteName = BASE_URL.split(".com")[0].split(".").pop();
const allPaths = {
  baselineDirPath: path.resolve(__dirname, `./../compareFiles/${siteName}/baselineDir/`),
  testDirPath: path.resolve(__dirname, `./../compareFiles/${siteName}/testDir/`),
  diffDirPath: path.resolve(__dirname, `./../compareFiles/${siteName}/diffDir/`)
};

let page;
let browser;
let fileName;
let URL;

// beforeSuite(async function() {
//   browser = await puppeteer.launch({
//   args: [
//   "--disable-gpu",
//   "--disable-dev-shm-usage",
//   "--disable-setuid-sandbox",
//   "--no-first-run",
//   "--no-sandbox",
//   "--no-zygote",
//   "--ignore-certificate-errors"
//   ],
//   headless: false
//   });
  
//   page = await browser.newPage();
//   await page.setViewport({ width: 1400, height: 900 });
//   fileSystemUtil.mkDirPath(allPaths.baselineDirPath);
//   fileSystemUtil.mkDirPath(allPaths.testDirPath);
//   fileSystemUtil.mkDirPath(allPaths.diffDirPath);
// });

// step("Open browser", async function () {
//   browser = await puppeteer.launch({
//     args: [
//       "--disable-gpu",
//       "--disable-dev-shm-usage",
//       "--disable-setuid-sandbox",
//       "--no-first-run",
//       "--no-sandbox",
//       "--no-zygote",
//       "--ignore-certificate-errors"
//     ],
//     headless: false
//   });

//   page = await browser.newPage();
//   await page.setViewport({ width: 1400, height: 900 });
//   fileSystemUtil.mkDirPath(allPaths.baselineDirPath);
//   fileSystemUtil.mkDirPath(allPaths.testDirPath);
//   fileSystemUtil.mkDirPath(allPaths.diffDirPath);
// });

// afterSuite(async function() {
//   browser.close();
// });
  
// step("Close browser", async function () {
//   browser.close();
// });

step("Go to <relativeUrl>", async function(relativeUrl) {
  fileName = relativeUrl
    .replace(/^\//g, siteName)
    .replace(/[\/\?]/g, "-")
    .replace(/(^-)|(\.|-)$/g, "");
  // if (BASE_URL.includes("master.dev"))
  //   relativeUrl = `${relativeUrl}?fcsis=stage`;
  URL = `${BASE_URL}${relativeUrl}`;
  await page.authenticate({
    username: "preview@futurenet.com",
    password: "sisterrisogr"
    });
  const response = await page.goto(URL, {
    waitUntil: "networkidle0",
    timeout: 0
  });
  const { status } = response.headers();
  gauge.message(`Staus code for ${URL} is ${status}`);
  await page.waitFor(1000);
});


step("Create baseline data", async function () {
  const baseFilePath = `${allPaths.baselineDirPath}/${fileName}.txt`;
  const content = `URL: ${URL}${await fetchContentFromWebsite(page)}`;
  await fileSystemUtil.writeDataInFile(baseFilePath, content);
});

step("Create test file and compare it with baseline", async function () {
  const testFilePath = `${allPaths.testDirPath}/${fileName}.txt`;
  const baseFilePath = `${allPaths.baselineDirPath}/${fileName}.txt`;
  const diffFilePath = `${allPaths.diffDirPath}/${fileName}.txt`;
  const content = await fetchContentFromWebsite(page);
  await fileSystemUtil.writeDataInFile(testFilePath, content);
  const isEqual = await fileSystemUtil.compareFiles(
    baseFilePath,
    testFilePath,
    diffFilePath
  );
  assert.ok(isEqual, "Files are not equal, check console log for the errors");
});

async function fetchContentFromWebsite(page) {
  await puppeteerUtil.removeHtmlElementByJQuery(page, "img");
  await puppeteerUtil.removeHtmlElementByJQuery(page, "style");
  await puppeteerUtil.removeHtmlElementByJQuery(page, "script");
  await puppeteerUtil.removeHtmlElementByJQuery(page, "#popularcontent");
  await puppeteerUtil.removeHtmlElementByJQuery(
    page,
    "*[class*='hawk'] :not(p)"
  );
  await puppeteerUtil.removeHtmlElementByJQuery(page, ".cc-window.cc-banner");
  await puppeteerUtil.removeHtmlElementByJQuery(
    page,
    ".newsletter-offer-products"
  );
  await puppeteerUtil.removeHtmlElementByJQuery(
    page,
    "div.related-articles-block"
  );
  await puppeteerUtil.removeHtmlElementByJQuery(
    page,
    "div.read-more-container"
  );
  await puppeteerUtil.removeHtmlElementByJQuery(
    page,
    "#content-after-image"
  );
  await puppeteerUtil.removeHtmlElementByJQuery(
    page,
    ".future__jwplayer"
  );
  await puppeteerUtil.removeHtmlElementByJQuery(
    page,
    "div.um_ultimedia_wrapper_videoWrapper"
  );
  
  await puppeteerUtil.removeHtmlElementByJQuery(
    page,
    "div.um_ultimedia_wrapper_jcarousel"
  );
  
  await puppeteerUtil.removeHtmlElementByJQuery(
    page,
    "div#player-player"
  );
  await puppeteerUtil.removeHtmlElementByJQuery(
    page,
    "div#um_ultimedia_wrapper_wrap_widget_default"
  );
  await puppeteerUtil.removeHtmlElementByJQuery(page, ".no-wrap.relative-date");
  let content = await puppeteerUtil.getTextUsingJQuery(page, "body");
  content = await filterFile(content);
  return content;
}

async function filterFile(fileContent) {
  return new Promise(resolve => {
    fileContent = fileContent.replace(/^\s*/gm, "");
    fileContent = fileContent.replace(
      /(\s*<(img|iframe|a)\s*[^>]*>)|(<\/iframe>|<\/a>)/g,
      ""
    );
    fileContent = fileContent.replace(/^#tab-(.*)}/gm, "");
    fileContent = fileContent.replace(/^(?:[\t ]*(?:\r?\n|\r))+/gm, "");
    fileContent = fileContent.replace(/^Sign\s*Up(.*)permission./gm, "");
    fileContent = fileContent.replace(/(.)\s\nCLOSEX/gm, ".");
    fileContent = fileContent.replace(/[\s]$/g, "");
    fileContent = fileContent.replace(
      /^Advertisement\n\nAdvertisement\nAdvertisement/gm,
      ""
    );
    resolve(fileContent);
  });
}
