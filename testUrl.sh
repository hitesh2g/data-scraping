
today=`date '+%Y_%m_%d_%H:%M:%S'`;
logFile="logs/run_$today.log"
[ -f $logFile ] && rm -f $logFile

echo "Tests are running..."
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>"

{
 gauge run --tags test --table-rows "1"
} > $logFile

echo "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
echo "Please check log file in the logs folder "
echo "Please check index.html file in the reports folder"
