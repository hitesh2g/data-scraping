URL: https://master.dev.fte.tomsguide.com/logo
Created with Sketch.
Tom's Guide 
Search
RSS
Best Picks
News
Phones
TVs
Security
Reviews
More  
Antivirus
Audio
Cameras
Gaming
Home Networking
Smart Home
Software
Streaming
Wearables
What To Watch
How To
Hands-On
Forums
Trending
Best 4K Gaming TVs
iPhone 11 Rumors
Xbox Project Scarlett
Google Stadia
Sky Q Tips and Tricks
Best PS4 Deals
Square Enix E3 2019
Latest News
New Tech Charges 4,000mAh Phone Battery In Just 13 Minutes
By
Jesus Diaz 
a day ago
Developed by Vivo, the 120W fast charge technology only takes five minutes to charge the same battery to 50%.
Best Netflix UK Movies and TV Shows - Evangelion, The Raid, Aggretsuko and More
By
Richard Priday 
a day ago
So many shows, so little time. Make sure you don't waste yours by checking our recommendations here.
Nintendo, Sony and Microsoft Rail Against Trump's Console Tariffs
By
Kate Kozuch 
2 days ago
The three companies claim the federal government’s new tariffs on their products will cause a "ripple effect" across the tech industry.
Two Weeks to Live: What to Know About the Next Maisie Williams (Arya Stark) Show
By
Henry T. Casey 
2 days ago
Want more Arya Stark? For now, you're going to need to settle for Maisie Williams kicking butt under a new name and character.
Steam Summer Sale 2019: The Best Deals So Far
By
Hunter Fenollol 
2 days ago
Steam has officially kicked off its 2019 summer sale. From June 25 through July 9, you can expect some major deals on games from AAA's to indie darlings from "The Steam Grand Prix Summer Sale".
Verizon's Visible Now Offering $40 Unlimited Plan with No Speed Cap
By
Adam Ismail 
2 days ago
Existing and new customers will get to keep uncapped their data speeds forever, but you'll have to act fast if you want to get in on the deal.
6 Cheap Earbuds (Under $25) Ranked Best to Worst
We tested earbuds that are selling for less than $20 and ranked them from best to worst. See our favorites and our favorite earbuds overall.
New Pixel 4 Renders Reveal Multiple Rear Cameras
A Pixel 4 render reportedly based on early prototype schematics features a camera module capable of holding multiple lenses.
9 Cheap Noise-Canceling Headphones (Under $200), Ranked Best to Worst
Good active noise-canceling headphones don't need to cost an arm and a leg. Based on our testing, here are the top Amazon best-sellers, ranked from best to worst. 
OnePlus 7 Rumors: Release Date, Specs, Price and More
Rumors are heating up about the next flagship phone from OnePlus. Here's a roundup of what we're hearing about the OnePlus 7, and its specs, features and price.
Get the Excellent Mohu Leaf Metro For Just $10
There's never been a better time to cut the cord. Mohu's epic sitewide sale takes 40% off our favorite TV antennas. 
Sonos Beam Review: An Excellent Alexa Soundbar
The feature-packed Sonos Beam delivers very good sound for a small soundbar, and comes with Alexa voice controls. 
News
View more  
New Tech Charges 4,000mAh Phone Battery In Just 13 Minutes
By
Jesus Diaz 
a day ago
Developed by Vivo, the 120W fast charge technology only takes five minutes to charge the same battery to 50%.
Best Netflix UK Movies and TV Shows - Evangelion, The Raid, Aggretsuko and More
By
Richard Priday 
a day ago
So many shows, so little time. Make sure you don't waste yours by checking our recommendations here.
Best Picks
View more  
Best Identity-Theft Protection 2019
By
Paul Wagenseil, 
Brian Nadel 
a day ago
We tested the six most popular identity theft protection services over a three month process. Here are the services that protect you the best.
Best Phones of 2019
By
Mark Spoonauer 
2 days ago
Here are the best smartphones that we’ve tested, with the top Android phones and iPhones, and our favorites for budget shoppers.
Best Smart Light Switches of 2019
By
Mike Prospero 
2 days ago
Smart switches connect to your home Wi-Fi network, letting you control your lights with a phone, tablet or voice-controlled smart speaker.
Phones
View more  
Will Robocalls Finally Be Banned? What You Need to Know
By
Monica Chin 
21 days ago
You may soon be free from pesky robocalls, but you may need to pay.
Facetune 2: Best Portrait-Retouching App
By
Sally Wiener Grotta 
May 22, 2019
Make your mug look magnificent.
Black Shark 2 Review: The Gaming Phone You Really Want
By
Adam Ismail 
April 15, 2019
Chinese gaming-phone startup Black Shark is gunning for OnePlus' bargain flagship crown, and it nearly pulls off the heist with a sleek design and stunning performance.
TVs
View more  
Best Deals on TVs in June 2019
By
Hilda Scott 
2 days ago
From 4K TVs to budget QLED sets, we're tracking the Internet's best TV deals.
Best 4K Ultra-HD TVs 2019
By
Brian Westover 
2 days ago
Ultra HD TVs are getting more affordable, giving you a very sharp picture for less money. Here are the best 4k TVs available now.
Smart TVs: Everything You Need to Know
By
Brian Westover, 
John Quain 
2 days ago
A smart TV makes it easy to stream movies and shows, and newer models offer voice control and smart home integration. But there are some risks, too.
Security
View more  
Best Identity-Theft Protection 2019
By
Paul Wagenseil, 
Brian Nadel 
a day ago
We tested the six most popular identity theft protection services over a three month process. Here are the services that protect you the best.
LifeLock Ultimate Plus: Tops in Identity Protection
By
Brian Nadel 
2 days ago
LifeLock offers the most comprehensive personal-data monitoring and a great deal on antivirus software, but it's pricey.
IDShield Identity Protection: A Great Bargain
By
Brian Nadel 
2 days ago
IDShield's frequent three-bureau credit reports and scores are great bargains, though IdentityForce and LifeLock monitor more personal data.
Reviews
View more  
Visible Review: Is This Carrier's $40 Unlimited Plan Too Good to Be True?
By
Philip Michaels 
a day ago
Verizon-owned Visible offers unlimited LTE smartphone data for just $40 a month — a bargain price if you don't mind using the iPhone.
LifeLock Ultimate Plus: Tops in Identity Protection
By
Brian Nadel 
2 days ago
LifeLock offers the most comprehensive personal-data monitoring and a great deal on antivirus software, but it's pricey.
IDShield Identity Protection: A Great Bargain
By
Brian Nadel 
2 days ago
IDShield's frequent three-bureau credit reports and scores are great bargains, though IdentityForce and LifeLock monitor more personal data.
Identity Guard Premier: Smarter but Sparser
By
Brian Nadel 
2 days ago
Identity Guard Premier's selling point is IBM's Watson artificial intelligence, but that doesn't make up for the fairly paltry feature set.
IdentityForce UltraSecure+Credit: Best Identity-Protection Service Overall
By
Brian Nadel 
2 days ago
IdentityForce offers the best overall service when credit monitoring is factored in, but the more expensive LifeLock has the edge on pure identity protection.
Privacy Guard Total Protection: Most Useful Tools
By
Brian Nadel 
2 days ago
PrivacyGuard has the most useful tools and most frequent credit-file information, but other services monitor more types of personal data.
Tom's Guide is part of Future US Inc, an international media group and leading digital publisher. Visit our corporate site.
Terms and conditions
Privacy policy
Cookies policy
Advertise
About us
Contact us
Archives
©
Future US, Inc. 
11 West 42nd Street, 15th Floor,
New York,
NY 10036. 