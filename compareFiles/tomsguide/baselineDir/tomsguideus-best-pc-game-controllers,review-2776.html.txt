URL: https://master.dev.fte.tomsguide.com/us/best-pc-game-controllers,review-2776.htmllogo
Created with Sketch.
Tom's Guide 
Search
RSS
Best Picks
News
Phones
TVs
Security
Reviews
More  
Antivirus
Audio
Cameras
Gaming
Home Networking
Smart Home
Software
Streaming
Wearables
What To Watch
How To
Hands-On
Forums
Trending
Best 4K Gaming TVs
iPhone 11 Rumors
Xbox Project Scarlett
Google Stadia
Sky Q Tips and Tricks
Best PS4 Deals
Square Enix E3 2019
Best Picks
Best PC Game Controllers 2019
Best PC Game Controllers 2019
By
Andrew Melcon, 
Michael Andronico
Console Gaming 
Here are the best PC controllers for your favorite games, from retro pads to high-end peripherals for pro gaming.
Shares 
Page 1 of 2:
Page 1
Page 1
Page 2
Best PC Game ControllersWhile many people swear by mouse and keyboard for most PC games, titles such as Mortal Kombat 11 and Sekiro: Shadows Die Twice are simply meant to be played with a PC controller. We tested over a dozen of the most popular PC-friendly gamepads out there in a variety of genres to help you find the perfect fit for your playstyle. Our top overall pick is Microsoft's Xbox One controller, thanks to its comfortable design, impressive build quality and instant compatibility with pretty much every PC game out there.But there are plenty of other great options available, from the retro-inspired 8BitDo SN30 to beefed-up premium options like the Xbox One Elite Wireless controller. Astro's $199 C40 TR just arrived as a solid competitor to the Xbox Elite controller, offer a finer degree of physical customization and an impressively solid build quality. Other options such as the SteelSeries Stratus Duo even let you stay connected to your PC and mobile device at the same time.No matter your needs, here are the best PC controllers to buy now. 
Best Overall
Microsoft Xbox One Controller
Microsoft's Xbox One controller is the gold standard for PC gaming, thanks to its ergonomic design and intuitive button layout. It's plug-and-play with nearly all modern PC games, and sports Bluetooth for easy wireless use.
Best Under $30
Logitech F310
The Logitech F310 is a great option for gamers on a budget, offering a solid d-pad, responsive buttons and support for a wide range of titles, all for less than $30.
Best for Serious Gamers
Xbox One Elite Wireless Controller
Looking to go pro? The Xbox One Elite Wireless Controller is a premium version of the traditional Xbox pad, sporting a heftier, soft-touch design, tweakable triggers and a variety of swappable components.
1
2
Current page:
Page 1
Next Page Page 2
by Taboolaby TaboolaSponsored LinksSponsored LinksPromoted LinksPromoted LinksYou May LikeupGradWhy Online MSc in Machine Learning, Seems More Affordable?upGradUndoshoptosave.inMens Watchshoptosave.inUndoUpGrad &amp; IIITBLaunch Your Career in Data Science with IIIT Bangalore. Learn From Top Experts & Faculty. Industry-Relevant Curriculum. Apply Now!UpGrad & IIITBUndoKENT RO Systems Ltd.Why Smart Families Are Buying Next-Gen Kent RO?KENT RO Systems Ltd.UndoCoverfox.comGet Protection Against 40+ Critical Illnesses with Term InsuranceCoverfox.comUndoPolicybazaar.com1 Crore Term Insurance Plan @ ₹490/Month. Covers Upto 85 Years + 59 Critical illnesses.Policybazaar.comUndo
Tom's Guide is part of Future US Inc, an international media group and leading digital publisher. Visit our corporate site.
Terms and conditions
Privacy policy
Cookies policy
Advertise
About us
Contact us
Archives
©
Future US, Inc. 
11 West 42nd Street, 15th Floor,
New York,
NY 10036. 