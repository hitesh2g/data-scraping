URL: https://master.dev.fte.tomsguide.com/us/car-tech-2015,review-1378.htmllogo
Created with Sketch.
Tom's Guide 
Search
RSS
Best Picks
News
Phones
TVs
Security
Reviews
More  
Antivirus
Audio
Cameras
Gaming
Home Networking
Smart Home
Software
Streaming
Wearables
What To Watch
How To
Hands-On
Forums
Trending
Best 4K Gaming TVs
iPhone 11 Rumors
Xbox Project Scarlett
Google Stadia
Sky Q Tips and Tricks
Best PS4 Deals
Square Enix E3 2019
Special Report
Future Tech: Your Car In 2015
Future Tech: Your Car In 2015
By
Bruce Gain
Automotive 
Car-makers fight to stay alive during the recession by packing new models with technical wizardry. Cars are about to get smarter, safer, and cooler.
Shares 
Page 1 of 16:
Future Tech: Your Car In 2015
Future Tech: Your Car In 2015
Bot Pulls Car Over to Safety
One Microchip, One Car Rental for 30 Minutes
Surround Engine Sound
Programmable Key for Safer Teen Driving
Ignition Self Starts and Turns Off in Stop-and-Go Traffic
Drone Cars
Stop That Car-With a Remote
Proactive Crash Protection
Car Avoids Hitting Pedestrians Before You Even See Them
Scans Road Ahead, Adjusts Chassis
Four-Point Collision Prevention
The Talking Car
Don’t Run That Red Light
Detecting Hidden Obstacles at Night
Smarter 3D Navigation
IntroductionThe car industry is in the middle of The Great Depression 2.0—but that doesn't mean that automakers have shut down their R&D labs. If anything, car makers seek to survive by packing new models with technical wizardry in order to hang on to their share of a shrinking market. What that means is that one can expect cars to become a lot smarter, safer, and ultimately, cooler, during the next three to five years. It may be a long time before we can pilot cars that fly in the air like in the classic Hanna-Barbera cartoon the Jetsons, but you can expect your automobile to be able to do things that might have seemed unimaginable just a few years ago.New tech usually shows up in luxury car models, and since German car makers dominate this sector, a lot of what we will see and drive in the future comes from this corner of the world. Still, there are plenty of neat things that you will see in other models from other countries as well.
1
2
3
4
5
6
7
8
…
16
Current page:
Future Tech: Your Car In 2015
Next Page Bot Pulls Car Over to Safety
by Taboolaby TaboolaSponsored LinksSponsored LinksPromoted LinksPromoted LinksYou May LikeupGradWhy Online MSc in Machine Learning, Seems More Affordable?upGradUndoshoptosave.inMens Watchshoptosave.inUndoKENT RO Systems Ltd.Why Smart Families Are Buying Next-Gen Kent RO?KENT RO Systems Ltd.Undohear.comGurgaon: German founders reveal hearing aid secrethear.comUndoUpGrad &amp; IIITBLaunch Your Career in Data Science with IIIT Bangalore. Learn From Top Experts & Faculty. Industry-Relevant Curriculum. Apply Now!UpGrad & IIITBUndoPolicybazaar.com1 Crore Term Insurance Plan @ ₹490/Month. Covers Upto 85 Years + 59 Critical illnesses.Policybazaar.comUndo
Advertisement
Be In the Know
Get instant access to breaking news, the hottest reviews, great deals and helpful tips.
No spam, we promise. You can unsubscribe at any time and we'll never share your details without your permission.
Advertisement
Advertisement
Tom's Guide is part of Future US Inc, an international media group and leading digital publisher. Visit our corporate site.
Terms and conditions
Privacy policy
Cookies policy
Advertise
About us
Contact us
Archives
©
Future US, Inc. 
11 West 42nd Street, 15th Floor,
New York,
NY 10036. 