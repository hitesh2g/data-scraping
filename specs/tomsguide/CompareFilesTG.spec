# Baseline creation

tags: compareWithBaselineTG

   |relativeUrl                                                             |
   |------------------------------------------------------------------------|
   |/                                                             |
   |/articles                                                     |
   |/us/fidget-spinners-where-to-buy,news-25002.html              |
   |/us/galaxy-s8,review-4287.html                                |
   |/us/macos-security-suite-av-test,news-25413.html              |
   |/us/best-e-readers,review-2766.html                           |
   |/us/alienware-x51-r3,review-3088.html                         |
   |/us/game-of-thrones-vr,news-22546.html                        |
   |/us/alienware-x51-r3,review-3088.html                         |
   |/t/amazon/                                                    |
   |/us/lists/gift-ideas                                          |
   |/us/how-to-cancel-apple-news-plus,news-29810.html             |
   |/us/best-playstation-vue-deals,news-29930.html                |
   |/us/honor-lost-phone-reward,news-29934.html                   |
   |/us/apple-security-ios-9,news-21076.html                      |
   |/us/apple-wwdc-2015-preview,news-20773.html                   |
   |/us/galaxy-s6-camera-sensors-different,news-20891.html        |
   |/us/pain-relief-devices-tested,news-24683.html                |
   |/us/innovation-award-2016-lenovo,review-4101.html             |
   |/us/pictures-story/625-best-wearables-ces-2014.html           |
   |/us/hacking-internet-of-things,review-1901.html               |
   |/us/car-tech-2015,review-1378.html                            |
   |/us/jaybird-run-xt-vs-jabra-elite-active-65t,review-6370.html |
   |/us/galaxy-s8-vs-oneplus-5t,review-4993.html                  |
   |/us/alienware-gaming-pc-comparison,review-4201.html           |
   |/us/nexus-6-comparison,news-19774.html                        |
   |/us/ue-boom-vs-bose-soundlink-mini,review-2292.html           |
   |/us/best-pc-game-controllers,review-2776.html                 |
   |/us/sonos-beam-faq,review-5524.html                           |
   |/us/facebook-cambridge-data-trump,news-26800.html             |
   |/us/rooting-and-bootloader-explanation,news-21260.html        |
   |/us/router-terminology-guide,review-106.html                  |
   |/us/seemecnc-orion-delta-3d-printer,review-2813.html          |
   |/us/bose-soundlink-color,review-2795.html                     |
   |/us/trend-micro-antivirus-plus-security,review-2567.html      |
   |/us/photojojo-iphone-android-phone-lenses,review-2365.html    |
   |/us/audio-technica-ath-anc70-headphones,review-2142.html      |

## Create new baseline data
* Go to <relativeUrl>
* Create test file and compare it with baseline