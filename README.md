## General setup
```sh
$ npm install
```

## Run tests

To run test using tag

```sh
$ ./node_modules/.bin/gauge run -v -t baseline
```

```sh
$ ./node_modules/.bin/gauge run -v -p -t baseline
```

To run test using spec file

```sh
$ ./node_modules/.bin/gauge run -v specs/BaselineCreation.spec
```
To run a perticular row of table
gauge run -v -t ampValidation-ls --table-rows "1"
