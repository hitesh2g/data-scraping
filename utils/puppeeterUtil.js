const puppeeterUtil = {
  async injectJQuery(page) {
    await page.addScriptTag({
      url: "https://code.jquery.com/jquery-3.2.1.min.js"
    });
  },

  async getTextUsingJQuery(page, query) {
    await this.injectJQuery(page);
    const response = await page.evaluate(query => {
      const $ = window.$;
      return $(query).text();
    }, query);
    return response;
  },

  async removeHtmlElementByJQuery(page, query) {
    await this.injectJQuery(page);
    await page.evaluate(query => {
      const $ = window.$;
      $(query).remove();
    }, query);
  }
};

module.exports = puppeeterUtil;
