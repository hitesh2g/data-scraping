/* globals gauge*/
require("colors");
const fs = require("fs");
const Diff = require("diff");
const path = require("path");

function fileExistsSync(expectedPath) {
  try {
    const status = fs.lstatSync(expectedPath);
    status.isFile();
    return true;
  } catch (e) {
    return false;
  }
}

function mkDirPath(pathToCreate) {
  pathToCreate.split(path.sep).reduce((prevPath, folder) => {
    const currentPath = path.join(prevPath, folder, path.sep);
    if (!fs.existsSync(currentPath)) {
      fs.mkdirSync(currentPath);
    }
    return currentPath;
  }, "");
}

function writeDataInFile(filePath, content) {
  fs.writeFileSync(filePath, content, "utf8");
}

function compareFiles(firstFilePath, secondFilePath, diffFilePath) {
  return new Promise(resolve => {
    let actualFileContent = readFile(firstFilePath);
    let expectedFileContent = readFile(secondFilePath);
    deleteFile(diffFilePath);
    let diff = Diff.diffWordsWithSpace(actualFileContent, expectedFileContent);
    diff.forEach(function(part) {
      var color = part.added ? "green" : part.removed ? "red" : "grey";
      console.log(part.value[color])
      writeFileInAppendMode(diffFilePath, part.value[color]);
    });
    resolve(actualFileContent === expectedFileContent);
  });
}

function readFile(filePath) {
  return fs.readFileSync(filePath, "utf-8");
}

function deleteFile(filePath) {
  if (fileExistsSync(filePath)) fs.unlinkSync(filePath);
}

function writeFileInAppendMode(filePath, data) {
  fs.appendFileSync(filePath, data);
}

module.exports = {
  fileExistsSync,
  mkDirPath,
  writeDataInFile,
  compareFiles,
  writeFileInAppendMode,
  deleteFile
};
